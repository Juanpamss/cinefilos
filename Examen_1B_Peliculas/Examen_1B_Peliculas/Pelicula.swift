//
//  Pelicula.swift
//  App_Final
//
//  Created by Juan Pa on 7/2/18.
//  Copyright © 2017 Juan Pa. All rights reserved.
//

import Foundation
import ObjectMapper

class Pelicula: Mappable{
    
    var nombre:String?
    var descripcion:String?
    var director:String?
    var añoEstreno:String?
    var URLPoster:String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        
        nombre <- map["Title"]
        descripcion <- map ["Plot"]
        director <- map["Director"]
        añoEstreno <- map["Year"]
        URLPoster <- map["Poster"]
        
    }
    
}
