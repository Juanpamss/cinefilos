//
//  Actor.swift
//  App_Final
//
//  Created by Juan Pa on 7/2/18.
//  Copyright © 2017 Juan Pa. All rights reserved.
//

import Foundation
import ObjectMapper

class Actor: Mappable {
    
    var nombre:String?
    var biografia:String?
    var edad:String?
    var fechaNacimiento:String?
    var lugarNacimiento:String?
    var URLPoster:String?
    var idActor: Int?
    var resultado: [Resultados]?
    
    
    required init?(map: Map){
    
    }
    
    func mapping(map: Map){
        
        nombre <- map["name"]
        biografia <- map ["biography"]
        edad <- map["homepage"]
        fechaNacimiento <- map["birthday"]
        lugarNacimiento <- map["place_of_birth"]
        URLPoster <- map["profile_path"]
        idActor <- map["page"]
        resultado <- map["results"]
        
    }
    
}

class Resultados: Mappable {
    var id: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
    }
    
   
}






