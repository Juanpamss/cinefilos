//
//  ActorViewController.swift
//  App_Final
//
//  Created by Juan Pa on 7/2/18.
//  Copyright © 2017 Juan Pa. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ActorViewController: UIViewController {

    //MARK:- Outlets
    
    @IBOutlet weak var nombreActor: UILabel!
    @IBOutlet weak var posterActor: UIImageView!
    @IBOutlet weak var biografiaActor: UITextView!
    @IBOutlet weak var edadActor: UILabel!
    @IBOutlet weak var fechaNacimientoActor: UILabel!
    @IBOutlet weak var lugarNaciActor: UILabel!
    @IBOutlet weak var lblDescripcion: UILabel!
    @IBOutlet weak var lblEdad: UILabel!
    @IBOutlet weak var lblFechaNacimiento: UILabel!
    @IBOutlet weak var lblLugarNacimiento: UILabel!
    
    var nombActorBuscar:String?
    var idActorAPI:Int?
    var URLPosterActor:String?
    var urlID:String?
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nombActorBuscar = ViewController.Variables.nombreBusqueda
        consultarID()
        //consultarActor()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK:- Actions
    
    func downloadImage(_ uri : String, inView: UIImageView){
        
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil{
                if let data = responseData {
                    
                    DispatchQueue.main.async {
                        self.posterActor.image = UIImage(data: data)
                    }
                    
                }else {
                    print("no data")
                }
            }else{
                print(error)
            }
        }
        
        task.resume()
        
    }
    
    func consultarID(){
        
        let nombreSinEspacios = nombActorBuscar?.replacingOccurrences(of: " ", with: "+")
    
        let URL = "https://api.themoviedb.org/3/search/person?api_key=6bbd6a62a9669e4b810e62dbb1317282&query=\(nombreSinEspacios!)"
        
        Alamofire.request(URL).responseObject { (response: DataResponse<Actor>) in
            
            
            let actor = response.result.value
            
            
            if let resultado = actor?.resultado {
                for resul in resultado {
                    
                     self.idActorAPI = resul.id
                    
                    self.urlID = "https://api.themoviedb.org/3/person/\("\(resul.id!)")?api_key=6bbd6a62a9669e4b810e62dbb1317282&language=en-US"
                    
                    Alamofire.request(self.urlID!).responseObject { (response: DataResponse<Actor>) in
                        
                        
                        let actor = response.result.value
                        
                        DispatchQueue.main.async {
                            
                            self.nombreActor.text = actor?.nombre ?? ""
                            self.biografiaActor.text = actor?.biografia ?? ""
                            self.fechaNacimientoActor.text = actor?.fechaNacimiento ?? ""
                            self.lugarNaciActor.text = actor?.lugarNacimiento ?? ""
                            self.edadActor.text = actor?.edad ?? ""
                            
                            let poster = "https://image.tmdb.org/t/p/w180_and_h180_bestv2\(actor?.URLPoster ?? "")"
                            
                            print (poster)
                            
                            self.downloadImage(poster, inView: self.posterActor)
                            
                            
                        }
                        
                    }
                    
                }
            }
    
        }
       
        
    }
    

}
