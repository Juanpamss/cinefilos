//
//  PeliculaViewController.swift
//  App_Final
//
//  Created by Juan Pa on 7/2/18.
//  Copyright © 2017 Juan Pa. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class PeliculaViewController: UIViewController {
   
    //MARK:- Outlets
    
    @IBOutlet weak var nombrePeli: UILabel!
    @IBOutlet weak var descripcionPeli: UITextView!
    @IBOutlet weak var directorPeli: UILabel!
    @IBOutlet weak var fechaEstrenoPeli: UILabel!
    @IBOutlet weak var lblDescrip: UILabel!
    @IBOutlet weak var lblDirector: UILabel!
    @IBOutlet weak var lblFechaEstreno: UILabel!
    @IBOutlet weak var posterPelicula: UIImageView!
    
    var nombPeliculaBuscar:String?
    
    
    //MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nombPeliculaBuscar = ViewController.Variables.nombreBusqueda
        consultarPelicula()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    // MARK:- Actions
    
    func downloadImage(_ uri : String, inView: UIImageView){
        
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil{
                if let data = responseData {
                    
                    DispatchQueue.main.async {
                        self.posterPelicula.image = UIImage(data: data)
                    }
                    
                }else {
                    print("no data")
                }
            }else{
                print(error)
            }
        }
        
        task.resume()
        
    }
    
    func consultarPelicula(){
        
        let nombreSinEspacios = nombPeliculaBuscar?.replacingOccurrences(of: " ", with: "+")
        
        let URL = "https://www.omdbapi.com/?t=\(nombreSinEspacios!)&apikey=fdc5c213"
        
        Alamofire.request(URL).responseObject { (response: DataResponse<Pelicula>) in
            
            let pelicula = response.result.value
            
            DispatchQueue.main.async {
                self.nombrePeli.text = pelicula?.nombre ?? ""
                self.descripcionPeli.text = pelicula?.descripcion ?? ""
                self.directorPeli.text = pelicula?.director ?? ""
                self.fechaEstrenoPeli.text = pelicula?.añoEstreno ?? ""
                
                self.downloadImage(pelicula?.URLPoster ?? "", inView: self.posterPelicula)
                
            }
            
        }
        
    }


}
