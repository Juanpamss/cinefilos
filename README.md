Cinefilos!
===================


Cinefilos una aplicacion que te permite buscar informacion acerca de tus **peliculas** y **actores** favoritos .

----------


Peliculas
-------------

En la pantalla de peliculas se mostrara el nombre completo de la pelicula, el director , el a�o de estreno y una peque�a descripcion de la pelicula. Ademas de esto se muestra la portada de la pelicula 

> **Nota:**

> - De momento se tiene que buscar por el nombre exacto de la pelicula para que aparezca la informacion
> - Por el momento la informacion se muestra en ingles 


----------


Actores
-------------------

Si se busca por actor se muestra el nombre completo , el a�o de nacimiento , una peque�a biografia , la pagina oficial del actor (en el caso de que disponga) y el lugar de nacimiento. Ademas de esto se muestra una foto del actor/actriz 

> **Nota:**

> - Por el momento la biografia se muestra en ingles 


Desarrolladores


- Juan Pablo Mendieta 
- Jaime Yerovi
- Andrea Villacis
